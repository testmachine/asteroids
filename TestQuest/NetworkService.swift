//
//  NetworkService.swift
//  TestQuest
//
//  Created by Igor Tyukavkin on 22.08.17.
//  Copyright © 2017 Trinity Digital. All rights reserved.
//

import Foundation

//Описание АПИ. ENDPOINT: Список Asteroids по дате.
//https://api.nasa.gov/api.html

class NetworkService {
    
    static var urlComponents = URLComponents(string: "https://api.nasa.gov/neo/rest/v1/feed")!
    static let session = URLSession.shared
    static let api_key = "mnRXibKwjv8IKogwxu4ITik4lKy8B2ADyUBZpc13"
    
    static func asteroids(date: Date, completion: @escaping ([Asteroid]) -> Void) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.string(from: date)
        
        urlComponents.queryItems =
            [
                URLQueryItem(name: "api_key", value: api_key),
                URLQueryItem(name: "start_date", value: strDate),
                URLQueryItem(name: "end_date", value: strDate)
        ]
        
        let searchURL = urlComponents.url!
        session.dataTask(with: searchURL, completionHandler: { (data, _, _) in
            var asteroids: [Asteroid] = []
            
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                if let near_earth_objects = json?["near_earth_objects"] as? [String:Any] {
                    if let asteroidItems = near_earth_objects[strDate] as? [Any] {
                                        for case let asteroidJson in asteroidItems {
                                            if let asteroidJson = asteroidJson as? [String:Any] {
                                                if let asteroid = Asteroid(json: asteroidJson) {
                                                    asteroids.append(asteroid)
                                                }
                                            }
                                        }
                    }
                }
            }
            
            completion(asteroids)
        }).resume()
    }
    
}

