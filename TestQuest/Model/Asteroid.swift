//
//  Asteroid.swift
//  TestQuest
//
//  Created by MacBookPro on 04/11/2017.
//  Copyright © 2017 Trinity Digital. All rights reserved.
//

import Foundation

class Asteroid {
    
    var name: String
    var missDistanceKilometers: Double = 0.0
    var diameterMinMeters: Double = 0.0
    var diameterMaxMeters: Double = 0.0
    
    var averageDiamaterMeters: Double {
        return (diameterMinMeters + diameterMaxMeters) / 2
    }
    
    init?(json: [String: Any]) {
        name = json["name"] as! String
        if let closeApproachData = json["close_approach_data"] as? [Any],
            closeApproachData.count > 0 {
            if let closeApproachDataItem = closeApproachData.first as? [String:Any] {
                    if let missDistanceItem = closeApproachDataItem["miss_distance"] as? [String:Any] {
                        missDistanceKilometers = Double((missDistanceItem["kilometers"] as? String) ?? "") ?? 0.0
                    }
                
            }
        }
        
        if let estimated_diameter = json["estimated_diameter"] as? [String:Any],
        let meters = estimated_diameter["meters"] as? [String:Any],
        let diameter_min = meters["estimated_diameter_min"],
        let diameter_max = meters["estimated_diameter_max"] {
            diameterMinMeters = diameter_min as? Double ?? 0.0
            diameterMaxMeters = diameter_max as? Double ?? 0.0
        }
}

}
