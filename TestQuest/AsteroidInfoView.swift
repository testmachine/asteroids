//
//  AsteroidInfoView.swift
//  TestQuest
//
//  Created by MacBookPro on 07/11/2017.
//  Copyright © 2017 Trinity Digital. All rights reserved.
//

import UIKit

class AsteroidInfoView: UIView {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var minDiameterLabel: UILabel!
    
    @IBOutlet weak var maxDiameterLabel: UILabel!

}
