//
//  ViewController.swift
//  TestQuest
//
//  Created by Igor Tyukavkin on 22.08.17.
//  Copyright © 2017 Trinity Digital. All rights reserved.
//

/*
 
 Тестовое задание заключается в следующем:
 
 1. Необходимо получить список астероидов используя АПИ NASA.
    Как это сделать написано в NetworkService. Ключ АПИ там же.
 2. Астероиды необходимо нарисовать на экране в зависимости от их удаления от Земли, соблюдая относительные размеры (estimated_diameter) и относительное расстояние (miss_distance)
 3  Рисовать астероиды необходимо в линию. Приложение должно работать только в ландшафтной оринтации.
    Земля должна находиться с левой стороны, астероиды должны располагаться от нее в линию до правого края экрана. Желательно, чтобы расстояние до самого дальнего астероида было максимальной "X" координатой этого астероида, а все остальные рисовались равномерно, соблюдая относительные расстояния:
    | {Земля} о  О оо оОо  О о |, где | - края экрана, о/О - астероиды.
 4. По умолчанию список астероидов должен приходить на сегодняшнюю дату.
 5. Дожна быть возможность сменить дату на любую другую. (добавьте контролы сверху или снизу экрана)
 6. При нажатии на астероид, должна появляться информация о нем: Название, размеры и расстояние до Земли. Желательно, чтобы информация появлялась над самим астероидом или под ним, а астероид как-то выделялся.
 7. Приложение должно работать на всех устройствах (iPhone/iPad), поддерживающих iOS версии 9.0 или больше.
 8. Базовый набор картинок есть в Assets (можно использовать свои). 3 разных картинки астероида характеризуют их размеры: маленький, средний или большой.
 9. Чистота кода и красота интерфеса поощряются.
 
 Удачи!
 
 
 */

import UIKit

class ViewController: UIViewController {
    

    var asteroids: [Asteroid] = []
    var asteroidInfoView: AsteroidInfoView?
    var previouslySelectedAsteroid: UIImageView?
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        asteroidInfoView = UIView.fromNib()
        if let infoView = asteroidInfoView {
            infoView.isHidden = true
            view.addSubview(infoView)
        }
        
        datePicker.maximumDate = Date()
        
        fetchAsteroidsFor(date: Date())
    }
    
    @IBAction func dateChanged(_ sender: Any) {
        
        asteroids = []
        for view in self.view.subviews {
            if view.isKind(of:UIImageView.self) || view.isKind(of:AsteroidInfoView.self) {
                view.removeFromSuperview()
            }
        }
        
        fetchAsteroidsFor(date: datePicker.date)
    }
    
    func fetchAsteroidsFor(date: Date) {
        activityIndicator.startAnimating()
        NetworkService.asteroids(date: date) { [weak self] asteroids in
            self?.asteroids = asteroids
            DispatchQueue.main.async {
                self?.drawObjects()
                self?.activityIndicator.stopAnimating()
            }
        }
    }
    
    func drawObjects() {

        let screenWidth = view.bounds.width
        let screenHeight = view.bounds.height
    
        let earthDiameter = screenHeight / 2
        let asteroidMaxWidth = screenHeight / 4
        let asteroidMinWidth = screenHeight / 8
        
        if let infoView = asteroidInfoView {
            infoView.isHidden = true
            infoView.layer.cornerRadius = 8.0
            infoView.clipsToBounds = true
            view.addSubview(infoView)
        }

        var earth : UIImageView
        earth  = UIImageView(frame:CGRect(x: -earthDiameter / 2, y: earthDiameter / 2, width: earthDiameter, height: earthDiameter));
        earth.image = UIImage(named:"earth")
        self.view.addSubview(earth)
        
        let earthBoundX = earth.frame.origin.x + earthDiameter
        let overallAsteroidWidth = screenWidth - earthBoundX
        let maxDistanceAsteroid = asteroids.max {
            $0.missDistanceKilometers < $1.missDistanceKilometers
        }
        
        let asteroidMaxAverageDiameter = getMaxAverageDiameter()
        
        var x: CGFloat = earthBoundX
        var y: CGFloat
        for index in 0..<asteroids.count {
            let currentAsteroid = asteroids[index]
            let asteroidDistanceRatio = currentAsteroid.missDistanceKilometers / maxDistanceAsteroid!.missDistanceKilometers
            let asteroidDistance = overallAsteroidWidth * CGFloat(asteroidDistanceRatio)
            let asteroidSizeRatio = currentAsteroid.averageDiamaterMeters / asteroidMaxAverageDiameter
            
            var asteroidWidth = CGFloat(asteroidMaxWidth * CGFloat(asteroidSizeRatio))
            asteroidWidth = (asteroidWidth < asteroidMinWidth) ? asteroidMinWidth : asteroidWidth
            
            y = (screenHeight / 2) - (asteroidWidth / 2)
            x = earthBoundX + asteroidDistance - asteroidWidth / 2
            
            var asteroidImageView : UIImageView
            asteroidImageView  = UIImageView(frame:CGRect(x: x, y: y, width: asteroidWidth, height: asteroidWidth));
            asteroidImageView.image = setAsteroidImage(asteroid: currentAsteroid)
            asteroidImageView.tag = index
            self.view.addSubview(asteroidImageView)
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            asteroidImageView.isUserInteractionEnabled = true
            asteroidImageView.addGestureRecognizer(tapGestureRecognizer)
        }
        
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let selectedAsteroid = tapGestureRecognizer.view as! UIImageView
        selectedAsteroid.layer.opacity = 0.5
        
        let asteroidIndex = selectedAsteroid.tag
        let asteroidData = asteroids[asteroidIndex]
    
        if let infoView = asteroidInfoView {
            
            infoView.center = selectedAsteroid.center
            infoView.frame.origin.y -= (selectedAsteroid.frame.height / 2 + infoView.frame.height / 2)
            if infoView.frame.origin.x < 0 {
                infoView.frame.origin.x = 10
            }
            if infoView.frame.origin.x + infoView.frame.width > view.bounds.width {
                infoView.frame.origin.x = view.bounds.width - infoView.frame.width - 10
            }
            
            
            infoView.nameLabel.text = asteroidData.name
            infoView.minDiameterLabel.text = "\(formatDoubleToString(doubleValue: asteroidData.diameterMinMeters)) m"
            infoView.maxDiameterLabel.text = "\(formatDoubleToString(doubleValue: asteroidData.diameterMaxMeters)) m"
            infoView.distanceLabel.text = "\(asteroidData.missDistanceKilometers) km"

            if let previouslySelectedAsteroid = previouslySelectedAsteroid,
                infoView.isHidden == false {
                if selectedAsteroid == previouslySelectedAsteroid {
                    infoView.isHidden = true
                }
                previouslySelectedAsteroid.layer.opacity = 1.0
            } else {
                view.bringSubview(toFront: infoView)
                infoView.isHidden = false
            }
            
        }
        previouslySelectedAsteroid = selectedAsteroid

    }
    
    func formatDoubleToString(doubleValue:Double) -> String {
        return String(format: "%.2f", doubleValue)
    }
    
    func getMaxDistance() -> Double {
        let max = asteroids.max{$0.missDistanceKilometers < $1.missDistanceKilometers}
        return max?.missDistanceKilometers ?? 0.0
    }
    
    func getMaxAverageDiameter() -> Double {
        let max = asteroids.max{$0.averageDiamaterMeters < $1.averageDiamaterMeters}
        return max?.averageDiamaterMeters ?? 0.0
    }
    
    func setAsteroidImage(asteroid: Asteroid) -> UIImage? {
        let max = asteroids.max{$0.averageDiamaterMeters < $1.averageDiamaterMeters}
        let min = asteroids.min{$0.averageDiamaterMeters < $1.averageDiamaterMeters}
        let minAverageDiameter = min!.averageDiamaterMeters
        let maxAverageDiameter = max!.averageDiamaterMeters
        let averageDiameterDifference = maxAverageDiameter - minAverageDiameter
        
        var asteroidImage: UIImage?
        switch(asteroid.averageDiamaterMeters) {
            case (minAverageDiameter)..<(minAverageDiameter + averageDiameterDifference / 3):
            asteroidImage = UIImage(named: "asteroid1")
        case (minAverageDiameter + averageDiameterDifference / 3)..<(minAverageDiameter + (2 * averageDiameterDifference / 3)):
            asteroidImage = UIImage(named: "asteroid2")
        default:
            asteroidImage = UIImage(named: "asteroid3")
        }
        return asteroidImage
    }


}

